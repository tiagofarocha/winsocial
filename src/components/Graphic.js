import React, { Component } from 'react';
import {LineChart, Line, XAxis, YAxis, Tooltip, Legend} from 'recharts';

export default class Graphic extends Component {
    data = []
    constructor() {
        super();
        this.state = {
            data: [],
            actualYear: new Date().getFullYear(),
            futereYear: undefined
        }
    }
    componentDidUpdate(prevProps) {
        let data = [];
        this.props.graph.forEach(element => {
            if (element.round) {
                data.push(element.round)
            }
        });
        if (prevProps.data !== this.props.data) {
            let futereYear = (this.props.data.total_months / 12 ) + this.state.actualYear;
            this.setState({
                data: data,
                futereYear: futereYear
            })    
        }
        
    }
    render() {
        if (!this.props.data || !this.props.graph) {
            return;
        }

        let values = [];
        this.state.data.forEach(element => {
            var per = (element * 42) / 100;
            values.push({name: '', winsocial: element, previdencia: parseInt(element - per) })
        })

        let { data } = this.props;

        return (
            <div className="graphic">
                {
                this.props.canShowDataGraph ?
                    <div className="salary">
                        <div className="cash-win">
                            <p>Renda mensal vitalícia WinSocial de</p>
                            <p>R$ {data.lifelong_pension},00</p>
                        </div>
                        <div  className="cash-pub">
                            <p>Renda mensal previdência pública</p>
                            <p>R$ {parseInt(data.lifelong_pension - ((data.lifelong_pension * 42) / 100))},00</p>
                        </div>
                    </div> : null
                }
                <LineChart width={600} height={300} data={values}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Line type="monotone" dataKey="winsocial" stroke="#85d288" strokeDasharray="3 4 5 2" />
                <Line type="monotone" dataKey="previdencia" stroke="#4a90e2" strokeDasharray="3 4 5 2" />
                </LineChart>
                {
                    this.props.canShowDataGraph ?
                        <div className="years">
                            <div>
                                <p>{this.state.actualYear}</p>
                                <p>{this.state.futereYear}</p> 
                            </div>
                            <div>
                                <p>Previdência WinSocial</p>
                                <p>Previdência pública</p>
                            </div>
                        </div> : null
                }
            </div>
        );
    }
}