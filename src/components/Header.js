import React, { Component } from 'react';
import logo from '../images/logo.svg';
import main from '../images/main.png';
import shield from '../images/icon-shield.svg';
import rocket from '../images/icon-rocket.svg';
import Button from './Button';

export default class Header extends Component {
    
    onClickButton() {
        let targetElement = document.querySelector('#main');
        window.scrollTo(0, targetElement.offsetTop)
      }

    render() {
        return (
            <section className="header-section">
            <div className="main">
              <h1>
                <img src={logo} alt="logo"/>
              </h1>
              <h2 className="main-title">Você no controle da diabetes e do seu bolso!</h2>
              <p className="main-text">Conheça nossos planos de previdência<br/> exclusivos para quem tem diabetes!</p>
              <div>
                <Button
                  className="button-default scroll"
                  text="SIMULAR AGORA"
                  onClick={() => this.onClickButton()}
                />
                <div className="flex">
                    <div>
                        <img alt="" src={shield} />
                        <p><b>5.240+</b> <br/> Simulações feitas</p>
                    </div>
                    <div>
                        <img alt="" src={rocket} />
                        <p><b>Rendimento de 42% a mais</b><br/> que a previdência do governo</p>
                    </div>
                </div>
              </div>
            </div>
            <div className="container-logo">
              <img src={main} alt="logo"/>
            </div>
          </section>
        );
    }
}