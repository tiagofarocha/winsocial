import React, { Component } from 'react';
import footer from '../images/footer.png';
import phone from '../images/phone.png';
import army from '../images/army.png';
import hearth from '../images/icon-hearth.svg';
import phoneIcon from '../images/icon-phone.svg';
import msg from '../images/icon-msg.svg';
import store from '../images/store.png';

export default class Footer extends Component {
    render() {
        return (
            <section className="footer-section">
                <div className="footer-content">
                    <h2>Empoderamento na palma da mão <span><img alt="icon" className="icon" src={army}/></span></h2>
                    <p>Compartilhamos nosso conhecimento para que todos tenham informações que ajudam a viver mais e melhor! Baixe o app e tenha diversos benefícios.</p>
                    <div className="steps">
                        <div>
                            <img alt="icon" className="icon" src={hearth} />
                            <p>Organize seus dados de saúde em um só lugar.</p>
                        </div>
                        <div>
                            <img alt="icon" className="icon" src={phoneIcon} />
                            <p>Compartilhe seus dados com quem você quiser.</p>
                        </div>
                        <div>
                            <img alt="icon" className="icon" src={msg} />
                            <p>Receba conteúdos que te ajudam a viver mais e melhor.</p>
                        </div>
                    </div>
                    <img alt="icon" src={store} className="store" />
                </div>
                <img src={footer} className="img-footer" alt="footer" />
                <img src={phone} className="phone" alt="phone" />
            </section>
        );
    }
}