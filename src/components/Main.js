import React, { Component } from 'react';
import Graphic from './Graphic';
import Button from './Button';
import hand from '../images/hand.png';
import loading from '../images/loading.gif';

export default class Main extends Component {

    constructor() {
        super();
        this.state = {
          data: [],
          graph: [],
          canShowDataGraph: false,
          isLoading: false
        }
    }

    async requestDataGraphic() {
        this.setState({
          isLoading: true
        })
        const no_cors = 'https://cors-anywhere.herokuapp.com/';
        let response = await fetch(`${no_cors}http://pension-simulator.herokuapp.com/simulator?down_payment=0&actual_age=${this.actualAge.value}&pensionable_age=${this.futureAge.value}&monthly_payment=${this.moneyPerMonth.value}`);
        let data = await response.json();
    
        let dataGraph = [];
    
        data.array.forEach(element => {
          dataGraph.push(element)
        });
    
        this.setState({
          data: data,
          graph: dataGraph,
          canShowDataGraph: true,
          isLoading: false
        })
    
        console.log(data)
      }
    render() {
        return (
            <section id="main">
                <h2>Garanta sua<br/> tranquilidade financeira <span><img src={hand} alt="braço"/></span></h2>
                <p>
                    Assim como os picos de glicemia, imprevistos também <br/> acontecem!
                    Por isso ter um plano de previdência garante sua <br/> segurança financeira e a de quem você ama.
                </p>
                <div className="main-section">
                    <div className="flex">
                        <div>
                            <p>Você tem <input ref={actualAge => this.actualAge = actualAge} type="text"/> anos e quer <br/>
                            se aposentar com <input ref={futureAge => this.futureAge = futureAge} type="text"/> anos.</p>
                            <p>Com depósitos de pelo<br/> menos R$<input ref={moneyPerMonth => this.moneyPerMonth = moneyPerMonth} type="text"/>por mês.</p>
                        </div>
                        <Button
                            className="button-default"
                            text={this.state.isLoading ? <img src={loading} /> : "COMPRAR AGORA"}
                            onClick={() => this.requestDataGraphic()}
                        />
                    </div>
                    <div className="flex">
                        <Graphic
                            data={this.state.data}
                            graph={this.state.graph} 
                            canShowDataGraph={this.state.canShowDataGraph}
                        />
                    </div>
                </div>
            </section>
        );
    }
}