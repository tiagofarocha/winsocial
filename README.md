# Teste Técnico React

#### **A considerar-se**

* **tecnologias**: o stack fica a critério do candidato sendo indispensável o uso do React. Se valorizaria o uso de Gulp/SASS. O uso de Redux é opcional pela falta de necessidade mas seria apreciado.
* **entrega**: o projeto deve ser comitado até o dia 01/10 no repositório administrado pela WinSocial

## API Specification

### **Pension Simulator**
Returns the accumulated amount and the lifelong pension given the parameters.

#### **Request**
```
GET http://pension-simulator.herokuapp.com/simulator?simulator?down_payment=5000&monthly=237&actual_age=30&pensionable_age=65
```
* **down_payment**: aporte inicial;
* **monthly_payment**: depósitos mensais;
* **actual_age**: idade atual;
* **pensionable_age**: idade de aposentadoria;

#### **Response**
```
{
    "down_payment": 5000,
    "monthly_payment": 237,
    "actual_age": 30,
    "pensionable_age": 65,
    "total_months": 420,
    "money_accumulated": 233710,
    "lifelong_pension": 584,
    "array": []
}
```
* down_payment: aporte inicial
* monthly_payment: depósitos mensais
* actual_age: idade atual
* pensionable_age: idade de aposentadoria
* total_months: total de meses entre a idade atual e a idade de aposentadoria
* money_accumulated: total acumulado ao final do período
* lifelong_pension: renda mensal vitalícia
---

### **Instruções** 
A app é uma SPA. Esperamos fluidez na experiência do usuário e que o processo seja o mais orgânico possível. Sinta-se à vontade para imprimir a tua identidade no código. O botão SIMULAR JÁ faz um scroll até a app de simulação logo abaixo. Os valores do lado esquerdo devem ser inputados pelo usuário. Os valores do lado direito serão automaticamente atualizados a partir dos dados da resposta da api de simulação. Seria interessante que o gráfico também fosse dinâmico.